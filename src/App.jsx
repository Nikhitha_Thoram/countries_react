import { useState } from "react";
import "./App.css";
import HomePage from "./components/HomePage";
import ThemeProvider from "./components/ThemeProvider";
import CountryDetails from "./components/countryDetails";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ErrorPage from "./components/ErrorPage";

function App() {
  return (
    <ThemeProvider>
      <Router>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/country/:id" element={<CountryDetails />} />
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default App;
