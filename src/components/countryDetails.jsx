import React from "react";
import { useState, useEffect, useContext } from "react";
import { useParams, useNavigate, useLocation } from "react-router-dom";
import "../App.css";
import { ThemeContext } from "./ThemeProvider";
import Header from "./Header";

function CountryDetails() {
  const { id } = useParams();
  const [country, setCountry] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [allCountries, setAllCountries] = useState([]);
  const navigate = useNavigate();

  const { darkMode, toggleDarkMode } = useContext(ThemeContext);

  useEffect(() => {
    fetch(`https://restcountries.com/v3.1/alpha/${id}`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setCountry(data[0]);
        console.log(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching data", error);
        setError(error);
        setLoading(false);
      });
  }, [id]);

  useEffect(() => {
    fetch(`https://restcountries.com/v3.1/all`)
      .then((response) => response.json())
      .then((data) => {
        setAllCountries(data);
      })
      .catch((error) => {
        console.error("Error fetching all countries data", error);
        setError(error);
      });
  }, []);

  const getNativeName = () => {
    if (country && country.name && country.name.nativeName) {
      const firstKey = Object.keys(country.name.nativeName)[0];
      return country.name.nativeName[firstKey].official;
    }
    return null;
  };

  const renderLanguages = () => {
    if (!country || !country.languages) return null;
    return Object.values(country.languages).join(", ");
  };

  const getBorderCountryNames = () => {
    if (!country || !country.borders || country.borders.length === 0) {
      return [];
    }
    return country.borders.reduce((borderNames, borderCode) => {
      const borderCountry = allCountries.find(
        (item) => item.cca3 === borderCode
      );
      if (borderCountry) {
        borderNames.push(borderCountry.name.common);
      }
      return borderNames;
    }, []);
  };

  const borderCountryNames = getBorderCountryNames();

  return (
    <div
      className={`${
        darkMode
          ? "bg-[#202d36] text-white min-h-screen"
          : "bg-[#fafafa] text-black"
      }`}
    >
      <Header />
      <div className="body-section">
        {loading ? (
          <div className="loader"></div>
        ) : error ? (
          <div>Error: {error}</div>
        ) : country ? (
          <div>
            <button
              className={` ${
                darkMode
                  ? "bg-[#2B3743] text-white shadow-md"
                  : "bg-white text-black shadow-md"
              } back_button rounded-md mt-10 ml-32 px-5 py-2 items-center mb-20 w-32`}
              onClick={() => navigate(-1)}
            >
              Back
            </button>
            <div class="details-section pl-32 pr-40 flex flex-row gap-36">
              <div className="flag_display">
                <img
                  src={country.flags.svg}
                  alt={country.name.common}
                  className="w-[550px] h-[350px] object-cover"
                />
              </div>
              <div className="details_rightSection flex flex-col">
                <div className="common_details flex flex-row gap-60 text-nowrap  mt-8 leading-8">
                  <div className="left_content">
                    <h2 className="font-bold text-2xl mb-8">
                      {country.name.common}
                    </h2>
                    <p>
                      <span className="font-medium">Native Name:</span>{" "}
                      {getNativeName()}
                    </p>
                    <p>
                      <span className="font-medium">Population:</span>{" "}
                      {country.population}
                    </p>
                    <p>
                      <span className="font-medium">Region:</span>{" "}
                      {country.region}
                    </p>
                    <p>
                      <span className="font-medium">Sub Region:</span>{" "}
                      {country.subregion}
                    </p>
                    <p>
                      <span className="font-medium">Capital:</span>{" "}
                      {country.capital}
                    </p>
                  </div>
                  <div className="right_content mt-14">
                    <p>
                      <span className="font-medium">Top Level Domain:</span>{" "}
                      {country.tld && country.tld[0]}
                    </p>
                    {country.currencies &&
                      Object.keys(country.currencies).map((code) => (
                        <p key={code}>
                          <span className="font-medium">Currency:</span>{" "}
                          {country.currencies[code].name}
                        </p>
                      ))}
                    <p>
                      <span className="font-medium">Languages:</span>{" "}
                      {renderLanguages()}
                    </p>
                  </div>
                </div>
                <div className="bottom_content mt-12">
                  <p className="flex flex-row items-center gap-2">
                    <span className="font-medium">Border Countries:</span>

                    {borderCountryNames.length > 0 ? (
                      <div className="border-countries flex flex-wrap flex-row gap-2">
                        {borderCountryNames.map((borderName, index) => (
                          <div
                            key={index}
                            className={`${
                              darkMode
                                ? "bg-[#2B3743] text-white"
                                : "bg-white text-black border border-gray-300"
                            } border-country-box text-nowrap px-2 text-center py-1 w-28 rounded-md`}
                          >
                            {borderName}
                          </div>
                        ))}
                      </div>
                    ) : (
                      <div>No border countries</div>
                    )}
                  </p>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="text-center text-red-500 mt-10 font-serif text-2xl">
            No data available for this country.
          </div>
        )}
      </div>
    </div>
  );
}

export default CountryDetails;
