import React from "react";
import { ThemeContext } from "./ThemeProvider";
import { useContext } from "react";
import { Link } from "react-router-dom";

function CountryCard({ country, allCountries }) {
  const { darkMode } = useContext(ThemeContext);


  // const getCurrencies = () => {

  //   if (country && country.currencies) {
  //     const firstKey = Object.keys(country.currencies)[0];
  //     return country.currencies[firstKey].name;
  //   }
  //   return null;
  // };
   

// const getCurrencies=()=>{
//   if(country && country.currencies){
//     const currencyKeys = Object.entries(country.currencies)
//     const names = currencyKeys.reduce(([key,data])=>{
//       if(data && data.name){
//         acc.push(data.name)
//       }
//       return acc;
//     },[]);
//     return names;
//   }
//   return null;
// }

// console.log(getCurrencies());


const getCurrencies = () => {
  if (country && country.currencies) {
    const currencyKeys = Object.keys(country.currencies);
    const names = currencyKeys.reduce((acc, key) => {
      if (country.currencies[key].name) {
        acc.push(country.currencies[key].name);
      }
      return acc;
    }, []);
    return names;
  }
  return null;
};
 
getCurrencies();
  
  return (
    <div>
      <Link to={`/country/${country.ccn3}`} state={{ allCountries }}>
        <div
          className={`bg-${darkMode ? "[#2B3743]" : "white"} text-${
            darkMode ? "white" : "black"
          } ${darkMode ? "shadow-md" : "shadow-lg"} rounded-md w-72 m-4`}
        >
          <img
            src={country.flags.png}
            alt={country.name.common}
            className="w-full h-40 object-cover rounded-t-md"
          />
          <div className="p-6">
            <h2 className="text-lg font-semibold mb-2">
              {country.name.common}
            </h2>
            <div className="country_details mb-6">
              <p>
                <span className="font-medium">Population:</span>{" "}
                {country.population}
              </p>
              <p>
                <span className="font-medium">Region:</span> {country.region}
              </p>
              <p>
                <span className="font-medium  mb-6">Capital:</span>{" "}
                {country.capital}
              </p>
              <p>
                <span className="font-medium  mb-6">Area:</span> {country.area}
              </p>
              <p>
                <span className="font-medium  mb-6">SubRegion:</span>{" "}
                {country.subregion}
              </p>
              <p>
                <span className='font-medium mb-6'>currencies:</span>
              {getCurrencies()}
              </p>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
}

export default CountryCard;
