import React from "react";
import CountryCard from "./CountryCard";
import "../App.css";

function HomePageLogic({
  countrySearch,
  regionFilter,
  subRegionFilter,
  sortPopulation,
  sortArea,
  currency,
  countries,
  loading,
  error,
}) {
  let filteredCountries = countries;

  if (countrySearch) {
    filteredCountries = filteredCountries.filter((country) =>
      country.name.common.toLowerCase().includes(countrySearch.toLowerCase())
    );
  }

  if (regionFilter) {
    filteredCountries = filteredCountries.filter(
      (country) => country.region.toLowerCase() === regionFilter.toLowerCase()
    );
  }

  if (subRegionFilter) {
    filteredCountries = filteredCountries.filter(
      (country) =>
        country.subregion &&
        country.subregion.toLowerCase() === subRegionFilter.toLowerCase()
    );
  }

  if (sortPopulation) {
    filteredCountries.sort((a, b) => {
      if (sortPopulation === "ascending") {
        return a.population - b.population;
      } else {
        return b.population - a.population;
      }
    });
  }

  if (sortArea) {
    filteredCountries.sort((a, b) => {
      if (sortArea === "ascending") {
        return a.area - b.area;
      } else {
        return b.area - a.area;
      }
    });
  }
  

const getCurrencies = () => {
  const countryCurrencies = {};
  countries.forEach((country) => {
    if (country && country.currencies) {
      const currencyKeys = Object.keys(country.currencies);
      const names = currencyKeys.reduce((acc, key) => {
        if (country.currencies[key].name) {
          acc.push(country.currencies[key].name.toLowerCase());
        }
        return acc;
      }, []);
      countryCurrencies[country.name.common] = names;
    }
  });
  return countryCurrencies;
};

const countryCurrencies = getCurrencies();

if (currency) {
  filteredCountries = filteredCountries.filter((country) => {
    if (countryCurrencies[country.name.common]) {
      return countryCurrencies[country.name.common].includes(currency.toLowerCase());
    }
    return false;
  });
}


  return (
    <div>
      {loading ? (
        <div className="loader"></div>
      ) : error ? (
        <div className="text-center font-serif text-2xl text-red-500">
          Error fetching data
        </div>
      ) : (
        <div className="country-grid grid grid-cols-4 gap-x-16 pl-28 pr-28">
          {filteredCountries.length === 0 &&
          (countrySearch ||
            regionFilter ||
            subRegionFilter ||
            sortArea || currency||
            sortPopulation) ? (
            <div className="error_text text-center text-nowrap font-serif m-6 font-bold text-4xl text-red-500">
              No such countries found
            </div>
          ) : (
            filteredCountries.map((country) => (
              <CountryCard
                key={country.ccn3}
                country={country}
                allCountries={countries}
              />
            ))
          )}
        </div>
      )}
    </div>
  );
}

export default HomePageLogic;
