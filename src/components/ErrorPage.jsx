import React from 'react'

export default function ErrorPage() {
  return (
    <div className='error_page'>
        <p className='text-center text-2xl text-black mt-16'>The page you are looking for does not exist</p>
    </div>
  )
}
