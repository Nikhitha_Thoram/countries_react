import React from "react";
import { useContext } from "react";
import { ThemeContext } from "./ThemeProvider";

function Header() {
  const { darkMode, toggleDarkMode } = useContext(ThemeContext);

  return (
    <div>
      <header
        className={` ${
          darkMode
            ? "bg-[#2B3743] text-white shadow-md"
            : "bg-white text-black shadow-md"
        } nav flex flex-row justify-between items-center pt-8 pb-8 pl-32 pr-40 `}
      >
        <h2 className="header_text text-2xl font-bold">Where in the world?</h2>
        <button className="theme_button" onClick={toggleDarkMode}>
          {darkMode ? <>🔆 Light Mode</> : <>🌙 Dark Mode</>}
        </button>
      </header>
    </div>
  );
}

export default Header;
