import React from "react";
import HomePageLogic from "./HomePageLogic";
import { useState, useContext, useEffect } from "react";
import { ThemeContext } from "./ThemeProvider";
import Header from "./Header";
import SearchCountries from "./Dropdown/SearchCountries";
import Filter from "./Dropdown/Filter";
import SortComponent from "./Dropdown/SortComponent";


function HomePage() {
  const [regionFilter, setRegionFilter] = useState("");
  const [countrySearch, setCountrySearch] = useState("");
  const [subRegionFilter, setSubRegionFilter] = useState("");
  const [countries, setCountries] = useState([]);
  const [sortPopulation, setSortPopulation] = useState("");
  const [sortArea, setSortArea] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const[currency,setCurrency]=useState('');
  
  const { darkMode, toggleDarkMode } = useContext(ThemeContext);

  const handleRegionFilter = (event) => {
    setRegionFilter(event.target.value);
  };

  const handleSubregionFilter = (event) => {
    setSubRegionFilter(event.target.value);
  };

  const handleCurrencyFilter = (event) => {
    setCurrency(event.target.value);
  };

  const handleSortPopulation = (event) => {
    setSortPopulation(event.target.value);
  };

  const handleSortArea = (event) => {
    setSortArea(event.target.value);
  };

  const handleSearchInput = (event) => {
    setCountrySearch(event.target.value);
  };

  




  useEffect(() => {
    fetch("/countries.json")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setCountries(data);
        console.log(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching data");
        setError(error);
        setLoading(false);
      });
  }, []);

  const getSubRegions = () => {
    const subregions = countries.reduce((acc, country) => {
      if (
        country.region.toLowerCase() === regionFilter.toLowerCase() &&
        country.subregion
      ) {
        if (!acc.includes(country.subregion)) {
          acc.push(country.subregion);
        }
      }
      return acc;
    }, []);
    return subregions;
  };

  const subRegions = regionFilter && getSubRegions();

  const uniqueRegions = countries.reduce((acc, country) => {
    if (
      country.region !== undefined &&
      country.region !== null &&
      !acc.includes(country.region)
    ) {
      acc.push(country.region);
    }
    return acc;
  }, []);


  const currencies=countries.reduce((acc,country)=>{
    if (country && country.currencies) {
      const firstKey = Object.keys(country.currencies)[0];
      acc.push(country.currencies[firstKey].name);
    }
    return acc;
  },[]);

  const getUniqueCurrencies=currencies.reduce((acc,currency)=>{
    if(!acc.includes(currency)){
      acc.push(currency);
    }
    return acc;
  },[]);
  

   
  return (
    <div
      className={`${
        darkMode
          ? "bg-[#202d36] text-white min-h-screen"
          : "bg-[#fafafa] text-black"
      }`}
    >
      <Header />
      <main>
        <section className="body_section mt-10 ">
          <div className="filter_section flex flex-row justify-between items-center pl-32 pr-40">
            <SearchCountries handleSearchInput={handleSearchInput} />
            <Filter
              label={"region"}
              onChange={handleRegionFilter}
              data={uniqueRegions}
            />
            <Filter
              label={"subRegion"}
              onChange={handleSubregionFilter}
              data={Array.isArray(subRegions) ? subRegions : []}
            />

             <Filter
              label={"currency"}
              onChange={handleCurrencyFilter}
              data={getUniqueCurrencies}
            />

            <SortComponent value={"Area"} onChange={handleSortArea} />
            <SortComponent
              value={"population"}
              onChange={handleSortPopulation}
            />
          </div>

          <div className="cards-section mt-10">
            <HomePageLogic
              countrySearch={countrySearch}
              regionFilter={regionFilter}
              subRegionFilter={subRegionFilter}
              sortPopulation={sortPopulation}
              currency={currency}
              sortArea={sortArea}
              countries={countries}
              loading={loading}
              error={error}
            />
          </div>
        </section>
      </main>
    </div>
  );
}

export default HomePage;
