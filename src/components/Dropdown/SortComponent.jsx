import React from "react";
import { useContext } from "react";
import { ThemeContext } from "../ThemeProvider";

function SortComponent({ value, onChange }) {
  const { darkMode, toggleDarkMode } = useContext(ThemeContext);

  return (
    <div>
      <select
        onChange={onChange}
        className={`${
          darkMode
            ? "bg-[#2B3743] text-white shadow-md"
            : "bg-white text-black shadow-md"
        } select_sort pl-5 pr-5 pt-4 pb-4 rounded-md focus:outline-none focus:ring-2 border:none focus:ring-[#d1cdcd] text-serif text-sm w-48`}
        id="select_sort_area"
      >
        <option className="custom_option" value="" disabled selected>
          Sort by {value}
        </option>
        <option className="custom_option" value="ascending">
          Ascending
        </option>
        <option className="custom_option" value="descending">
          Descending
        </option>
      </select>
    </div>
  );
}

export default SortComponent;
