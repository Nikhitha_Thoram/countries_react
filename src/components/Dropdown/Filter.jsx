import React from "react";
import { useContext } from "react";
import { ThemeContext } from "../ThemeProvider";

function Filter({ onChange, data, label }) {
  const { darkMode, toggleDarkMode } = useContext(ThemeContext);

  return (
    <div>
      <select
        onChange={onChange}
        className={`${
          darkMode
            ? "bg-[#2B3743] text-white shadow-md"
            : "bg-white text-black shadow-md"
        } select_region pl-5 pr-5 pt-4 pb-4  rounded-md focus:outline-none focus:ring-2 border:none focus:ring-[#d1cdcd] text-serif text-sm w-48`}
        id="select_region"
      >
        <option className="custom_option" value="" disabled selected>
          Filter by {label}
        </option>
        {data.map((option, index) => (
          <option className="custom_option" key={index} value={option}>
            {option}
          </option>
        ))}
      </select>
    </div>
  );
}

export default Filter;
