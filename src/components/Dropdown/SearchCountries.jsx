import React from "react";
import searchPng from "../../assets/search.png";
import { useContext } from "react";
import { ThemeContext } from "../ThemeProvider";

function SearchCountries({ handleSearchInput }) {
  const { darkMode, toggleDarkMode } = useContext(ThemeContext);

  return (
    <div>
      <div className="relative">
        <input
          type="text"
          onChange={handleSearchInput}
          placeholder="Search for a country..."
          className={`${
            darkMode
              ? "bg-[#2B3743] text-white shadow-md"
              : "bg-white text-black shadow-md"
          } pl-10 pr-4  pt-4 pb-4  rounded-md focus:outline-none focus:ring-2 focus:ring-[#d1cdcd] focus w-96`}
        />
        <img
          src={searchPng}
          alt="searchIcon"
          className="w-5 h-5 absolute left-3 top-1/2 transform -translate-y-1/2"
        />
      </div>
    </div>
  );
}

export default SearchCountries;
